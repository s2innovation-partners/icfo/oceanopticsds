FROM centos:7
COPY maxiv.repo /etc/yum.repos.d/
RUN yum install -y epel-release && yum makecache
RUN yum install -y \
    python36-pytango \
    python3-pip \
    && yum clean all
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY OceanOpticsSTS/OceanOpticsSTS.py /
ENTRYPOINT ["python", "/OceanOpticsSTS.py"]
