import logging
from random import uniform

from tango.server import Device, attribute, device_property, DeviceMeta, command, run
from tango import DebugIt, DispLevel, AttrWriteType, DevState
from seabreeze.spectrometers import Spectrometer, list_devices
from seabreeze.cseabreeze._wrapper import SeaBreezeError  # pylint: disable=no-name-in-module

from OceanOpticsSTS.generate_data import generate_spectrum

debug_it = DebugIt(show_args=True, show_kwargs=True, show_ret=True)


class OceanOpticsSTS(Device):
    __metaclass__ = DeviceMeta

    # ---------------------------------
    #   Properties
    # ---------------------------------

    ReadPoints = device_property(
        dtype=int,
        default_value=1024,
        doc="number of collected entries for the Ocean Optics STS spectrometer"
    )

    ReadPeriod = device_property(
        dtype=int,
        default_value=1000,
        doc="Polling period for reading data in ms for the Ocean Optics STS spectrometer"
    )

    Simulated = device_property(
        dtype=bool,
        default_value=False,
        doc="""Data generation mode. If True data are randomly generated.
         If False data are obtained from Ocean Optics STS spectrometer"""
    )

    SerialNumber = device_property(
        dtype=str,
        mandatory=True,
        doc="Serial number of Ocean Optics STS spectrometer"
    )

    # ---------------------------------
    #   Global methods
    # ---------------------------------

    @debug_it
    def init_device(self):
        Device.init_device(self)
        self._spectrum = [0] * self.ReadPoints
        self._reference = [0] * self.ReadPoints
        self._background = [0] * self.ReadPoints
        self._wavelengths = [0] * self.ReadPoints
        self.set_state(DevState.ON)

        if not self.Simulated:
            try:
                self.spec = Spectrometer.from_serial_number(self.SerialNumber)
            except SeaBreezeError:
                self.set_state(DevState.FAULT)
                self.warn_stream("No device found")

    def delete_device(self):
        self.spec.close()

    # ---------------------------------
    #   Attributes
    # ---------------------------------
    spectrum_plot = attribute(label="actual spectrum plot",
                              dtype=(float,),
                              polling_period=ReadPeriod.default_value,
                              display_level=DispLevel.OPERATOR,
                              access=AttrWriteType.READ,
                              max_dim_x=2000,
                              fget="get_spectrum",
                              doc="the actual spectrum plot for the Ocean Optics STS spectrometer")

    reference_plot = attribute(label="reference plot",
                               dtype=(float,),
                               polling_period=ReadPeriod.default_value,
                               display_level=DispLevel.OPERATOR,
                               access=AttrWriteType.READ_WRITE,
                               max_dim_x=2000,
                               fget="get_reference",
                               fset="set_reference",
                               doc="the reference plot for the Ocean Optics STS spectrometer")

    background = attribute(label="background plot",
                           dtype=(float,),
                           polling_period=ReadPeriod.default_value,
                           display_level=DispLevel.OPERATOR,
                           access=AttrWriteType.READ_WRITE,
                           max_dim_x=2000,
                           fget="get_background",
                           fset="set_background",
                           doc="the background plot for the Ocean Optics STS spectrometer")

    wavelengths = attribute(label="wavelengths",
                            dtype=(float,),
                            polling_period=ReadPeriod.default_value,
                            display_level=DispLevel.OPERATOR,
                            access=AttrWriteType.READ,
                            max_dim_x=2000,
                            fget="get_wavelengths",
                            doc="the measured wavelengths for the Ocean Optics STS spectrometer")

    @debug_it
    def get_spectrum(self):
        self._spectrum = generate_spectrum(self.ReadPoints) \
            if self.Simulated else self.spec.intensities()

        self._spectrum = [self._spectrum[n] - self._background[n] for n in range(self.ReadPoints)]

        return self._spectrum

    @debug_it
    def get_wavelengths(self):
        return range(self.ReadPoints) if self.Simulated else self.spec.wavelengths()

    @debug_it
    def get_reference(self):
        return self._reference

    @debug_it
    def set_reference(self, new_reference):
        self._reference = new_reference

    @debug_it
    def get_background(self):
        return self._background

    @debug_it
    def set_background(self, new_background):
        self._background = new_background

    @command
    @debug_it
    def save_reference(self):
        """
        save spectrum as reference
        """
        self._reference = self._spectrum[:]

    @command
    @debug_it
    def save_background(self):
        """
        save spectrum as background
        """
        self._background = [uniform(0, 2) for _ in range(self.ReadPoints)] \
            if self.Simulated else self._spectrum[:]

    @command(
        dtype_in=int,
        doc_in='integration time'
    )
    @debug_it
    def set_integration_time(self, time):
        """
        set integration time
        """
        if not self.Simulated:
            try:
                self.spec.integration_time_micros(time)
            except:  # pylint: disable=bare-except
                self.warn_stream("Wrong integration time. Give value in range %s"
                                 % self.integration_time_micros_limits())


def main(args=None, **kwargs):
    logging.basicConfig(format='%(asctime)s %(levelname)s:%(message)s', level=logging.DEBUG)
    return run((OceanOpticsSTS,), args=args, **kwargs)


if __name__ == '__main__':
    main()
