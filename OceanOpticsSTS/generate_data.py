from math import pi, sin
from random import uniform


def generate_spectrum(number_points):
    coeff = uniform(0, 5)
    return [sin(2 * pi * interval / 200.0 * coeff) for interval in range(number_points)]
