# OceanOpticsSTS

## Before install

```
sudo apt install tango-common libboost-dev libboost-all-dev libusb-dev
pip3 install PyTango
```

First install seabreeze library:
```
pip3 install seabreeze
seabreeze_os_setup
reboot
```

And after reboot install OceanOpticsSTS device by executing:

```
python3 setup.py install
```