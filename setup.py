import setuptools

setuptools.setup(
    name="tangods-oceanopticsSTS",
    version="1.0.0",
    description="Device server for OceanOptics STS",
    author="S2innovation",
    author_email="contact@s2innovation.com",
    license="Proprietary",
    classifiers=[
        'License :: Other/Proprietary License'],
    packages=setuptools.find_packages(),
    entry_points={
        'console_scripts': [
            "OceanOpticsSTS=OceanOpticsSTS.OceanOpticsSTS:main",
        ]
    }

)

